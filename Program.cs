﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Segundo_parcial
{
    class Program
    {
        static void Partidos(string[] partidos)
        {
            
        }
        static void Main(string[] args)
        {
            Console.WriteLine("--------------------------------Sistema de Votaciones--------------------------------");
            Console.WriteLine("Resultado de los votos");

            double Part1 = 0;
            double Part2 = 0;
            double Part3 = 0;
            double Part4 = 0;

            string[] nombrespartidos = { "PSC", "PTA", "UV", "POP" };
            

            double TotalVotos = 0;
            double cantidadvotos=1000;
            var random = new Random();
            while (TotalVotos < cantidadvotos)
            {

               
                int votos = random.Next(9);
                switch (votos)
                {
                    case 1:
                        Part1++;
                        break;
                    case 2:
                        Part2++;
                        break;
                    case 3:
                        Part3++;
                        break;
                    case 4:
                        Part4++;
                        break;
                    case 5:
                        if (Part1 > 0)
                        {
                            Part1--;
                        }
                        break;
                    case 6:
                        if (Part2 > 0)
                        {
                            Part2--;
                        }
                        break;
                    case 7:
                        if (Part3 > 0)
                        {
                            Part3--;
                        }
                        break;
                    case 8:
                        if (Part4 > 0)
                        {
                            Part4--;
                        }
                        break;
                }
                TotalVotos = Part1 + Part2 + Part3 + Part4;
            }

            int cantidadpartidos = nombrespartidos.Length;
            int secuencia = 0;
            string[] ordenpartidos = new string[4];
            while (secuencia < 4)
            {
                int posicion = random.Next(0, 4);
                if (ordenpartidos[posicion] == null)
                {
                    ordenpartidos[posicion] = nombrespartidos[secuencia];
                    secuencia++;
                }

            }

            double[] partidos = { Part1, Part2, Part3, Part4 };
            Comparison<double> comparador = new Comparison<double>((numero1, numero2) => numero2.CompareTo(numero1));
            Array.Sort<double>(partidos, comparador);

            int p = 0;
            foreach (int numero in partidos)
            {
                Console.WriteLine($"{p+1}) {ordenpartidos[p]}:");
                Console.WriteLine($"   {numero} = {(numero / cantidadvotos) * 100}%");
                p++;
            }
            string[,] datos = new string[4, 5] { { "PSC:","Partido de la Seguridad Ciudadana","Fundado en 1938","Candidato: Santiago Rodriguez","Para: Presidencia"},
            { "PTA:","Partido Transhumanista Avanzado","Fundado en 2011","Candidato: Pablo Castillo","Para: Presidencia"},
            {"UV:","Unión por la Verdad","Fundado en 1980","Candidato: Miriam Ramirez","Para: Presidencia" },
            {"POP:","Partido de la Orientación Pública","Fundado en 1980","Candidato: Elizabeth Hernandez","Para: Presidencia"} };

            string solicitud = "s";
            while (solicitud != "N")
            {
                Console.WriteLine("-------------------------------------------------------------------------------------");
                Console.WriteLine("Escriba el partido de que quiera saber más ('n' para salir)");
               
                solicitud = Console.ReadLine();
                solicitud = solicitud.ToUpper();
                Console.WriteLine("-------------------------------------------------------------------------------------");
                switch (solicitud)
                {
                    case ("PSC"):
                        for (int i = 0; i < 5; i++)
                        {
                            Console.WriteLine(datos[0, i]);
                        }
                        break;
                    case ("PTA"):
                        for (int i = 0; i < 5; i++)
                        {
                            Console.WriteLine(datos[1, i]);
                        }
                        break;
                    case ("UV"):
                        for (int i = 0; i < 5; i++)
                        {
                            Console.WriteLine(datos[2, i]);
                        }
                        break;
                    case ("POP"):
                        for (int i = 0; i < 5; i++)
                        {
                            Console.WriteLine(datos[3, i]);
                        }
                        break;
                    case ("N"):
                        break;
                    default:
                        Console.WriteLine("Solicitud desconocida");
                        break;

                }
                
            }
            //double porcentaje1 = (Part1 / cantidadvotos) * 100;
            //double porcentaje2 = (Part2 / cantidadvotos) * 100;
            //double porcentaje3 = (Part3 / cantidadvotos) * 100;
            //double porcentaje4 = (Part4 / cantidadvotos) * 100;
            //Console.WriteLine(Part1);
            //Console.WriteLine(porcentaje1+"%");
            //Console.WriteLine(Part2);
            //Console.WriteLine(porcentaje2 + "%");
            //Console.WriteLine(Part3);
            //Console.WriteLine(porcentaje3 + "%");
            //Console.WriteLine(Part4);
            //Console.WriteLine(porcentaje4 + "%");


            //Console.ReadKey();
        }
    }
}
